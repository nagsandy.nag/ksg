import React, { createRef, useRef, useState } from 'react';
import { SafeAreaView, StyleSheet  } from 'react-native';
import MapView, { Circle, Marker } from 'react-native-maps';
import getDistance from 'geolib/es/getDistance';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';


const Map = () => {


    interface LatLong {
        latitude: number;
        longitude: number;
    }

    const mapRef = React.useRef(null);

    const [markers, setMarkers] = useState<LatLong[]>([
        {
            latitude: 22.6138,
            longitude: 88.4306
        },
        {
            latitude: 22.6437,
            longitude: 88.3777
        },
        {
            latitude: 22.6626,
            longitude: 88.4090
        },
        {
            latitude: 22.6313,
            longitude: 88.3980
        }
    ]);

    const CENTER: LatLong = {
        latitude: 22.7248,
        longitude: 88.4789,
    }

    const [centre,setCentre] = useState(CENTER)
    
    const RADIUS: number = 5000;

    const myRefs = useRef([]);
    myRefs.current = markers.map((element, i) => myRefs.current[i] ?? createRef());

    React.useEffect(() => {
        // checkPermission()
        requestPermission()
    }, []);

    const requestPermission = ()=> {
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
            if(result=='denied'){
                setCentre(CENTER);
            }else if(result=='granted'){
                Geolocation.getCurrentPosition(
                    (position) => {
                        let {latitude,longitude} = position.coords;
                      setCentre({ latitude, longitude })
                    },
                    (error) => {
                      // See error code charts below.
                      console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }else{
                return false;
            }
        });
    }

    const checkPermission = () => {
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then((result) => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('This feature is not available (on this device / in this context)');
                        break;
                    case RESULTS.DENIED:
                        console.log('The permission has not been requested / is denied but requestable');
                        break;
                    case RESULTS.LIMITED:
                        console.log('The permission is limited: some actions are possible');
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        break;
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        break;
                }
            })
            .catch((error) => {
                // …
            });
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>

            <MapView
                style={{ flex: 1 }}

                provider={'google'}
                initialRegion={{
                    ...centre,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                ref={mapRef}
            >
                <Circle
                    center={centre}
                    radius={RADIUS}
                    fillColor={'rgba(237, 85, 116,0.5)'}
                >

                </Circle>
                {markers.map((marker, index) => {
                    return (
                        <Marker
                            key={index}
                            coordinate={marker}
                            calloutOffset={{ x: -8, y: 28 }}
                            calloutAnchor={{ x: 0.5, y: 0.4 }}
                            draggable={true}
                            onDragEnd={(event) => {
                                let finalCordinate = {
                                    latitude: event.nativeEvent.coordinate.latitude,
                                    longitude: event.nativeEvent.coordinate.longitude
                                }
                                let calculatedDistance = getDistance(CENTER, finalCordinate, 1)
                                if (calculatedDistance > RADIUS) {

                                    showMessage({
                                        message: 'You have left the area',
                                        type: "danger",
                                    });
                                } else {

                                    showMessage({
                                        message: "You are inside the area",
                                        type: "success",
                                    });
                                }
                            }}
                        >
                        </Marker>
                    )
                })}
            </MapView>
            <FlashMessage position="top" />
        </SafeAreaView>
    );
}



const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
});

export default Map