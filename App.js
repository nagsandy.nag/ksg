import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Map from './screens/Map';
import Qr from './screens/Qr';

const Tab = createBottomTabNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen
          options={{
            tabBarIcon: (() => null),
            headerShown:false,
            tabBarLabelStyle:{
              fontSize:20
            }
          }}
          name="Map"
          component={Map}
        />
        <Tab.Screen
          options={{
            tabBarIcon: (() => null),
            headerShown:false,
            tabBarLabelStyle:{
              fontSize:20
            }
          }}
          name="Qr"
          component={Qr}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}