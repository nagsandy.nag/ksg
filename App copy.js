
import React, { createRef, useRef } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text
} from 'react-native';
import MapView, { Callout, CalloutSubview, Circle, Marker } from 'react-native-maps';
import getDistance from 'geolib/es/getDistance';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";


const App = () => {

  const mapRef = React.useRef(null);
  const markerRef = React.useRef(null);
  const [bubbleMessage, setBubbleMessage] = React.useState("This is inside range.")
  const [markers,] = React.useState([
    {
      latitude: 22.6138,
      longitude: 88.4306
    },
    {
      latitude: 22.6437,
      longitude: 88.3777
    },
    {
      latitude: 22.6626,
      longitude: 88.4090
    },
    {
      latitude: 22.6313,
      longitude: 88.3980
    },
  ]);

  const CENTER = {
    latitude: 22.6403,
    longitude: 88.4048,
  }
  const RADIUS = 5000;

  const myRefs = useRef([]);
  myRefs.current = markers.map((element, i) => myRefs.current[i] ?? createRef());

  return (
    <SafeAreaView style={{ flex: 1 }}>
      
      <MapView
        style={{ flex: 1 }}

        provider={'google'}
        initialRegion={{
          latitude: 22.6403,
          longitude: 88.4048,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        ref={mapRef}
      >
        <Circle
          center={CENTER}
          radius={RADIUS}
          fillColor={'rgba(237, 85, 116,0.5)'}
        >

        </Circle>
        {markers.map((marker, index) => {
          return (
            <Marker
              key={index}
              coordinate={marker}
              calloutOffset={{ x: -8, y: 28 }}
              calloutAnchor={{ x: 0.5, y: 0.4 }}
              draggable={true}
              onDragEnd={(event) => {
                let finalCordinate = {
                  latitude: event.nativeEvent.coordinate.latitude,
                  longitude: event.nativeEvent.coordinate.longitude
                }
                let calculatedDistance = getDistance(CENTER, finalCordinate, 1)
                if (calculatedDistance > RADIUS) {
                  
                  showMessage({
                    message: 'You have left the area',
                    type: "danger",
                  });
                } else {
                  
                  showMessage({
                    message: "You are inside the area",
                    type: "success",
                  });
                }
              }}
            >
              <Callout
                alphaHitTest
                tooltip
                onPress={e => {
                  if (
                    e.nativeEvent.action === 'marker-inside-overlay-press' ||
                    e.nativeEvent.action === 'callout-inside-press'
                  ) {
                    return;
                  }
                }}
                style={styles.customView}
              >
                <View style={{
                  flexDirection: 'column',
                  alignSelf: 'flex-start',
                }}>
                  <View style={{
                    width: 140,
                    flexDirection: 'row',
                    alignSelf: 'flex-start',
                    backgroundColor: '#4da2ab',
                    paddingHorizontal: 20,
                    paddingVertical: 12,
                    borderRadius: 6,
                    borderColor: '#007a87',
                    borderWidth: 0.5,
                  }}>
                    <View style={{
                      flex: 1,
                    }}>
                      <Text>{`${bubbleMessage}`}</Text>
                      <View

                        style={{
                          width: 'auto',
                          backgroundColor: 'rgba(255,255,255,0.7)',
                          paddingHorizontal: 6,
                          paddingVertical: 6,
                          borderRadius: 12,
                          alignItems: 'center',
                          marginHorizontal: 10,
                          marginVertical: 10,
                        }}
                      >
                        <Text>Click me</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{
                    backgroundColor: 'transparent',
                    borderWidth: 16,
                    borderColor: 'transparent',
                    borderTopColor: '#007a87',
                    alignSelf: 'center',
                    marginTop: -0.5,
                  }} />
                  <View style={{
                    backgroundColor: 'transparent',
                    borderWidth: 16,
                    borderColor: 'transparent',
                    borderTopColor: '#4da2ab',
                    alignSelf: 'center',
                    marginTop: -32,
                  }} />
                </View>

              </Callout>
            </Marker>
          )
        })}
      </MapView>
      <FlashMessage position="top" />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
